<?php 

require_once('includes/header.php');

$db = new PDO('mysql:host=localhost;dbname=eat_it', 'root','');
$db->setAttribute(PDO::ATTR_CASE,PDO::CASE_LOWER);
$db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

$user_id = $_SESSION['user_id'];

if(!isset($user_id)){
	header('Location: panier.php');
}
$select = $db->query("SELECT * FROM users WHERE id='$user_id'");
$s = $select->fetch(PDO::FETCH_OBJ);

?>

</head>
	<link rel="stylesheet" type="text/css" href="css/achat.css">
</head>
<body>
    <div id="container">
        <h1>Votre commande a été enregistrée !</h1>
        <h2><?php echo $s->username; ?></h2>
        <p>Livraison prévue dans : 15 minutes !</p>
        <img id="imageprofile" src="img/LIVREUR.png">
    </div>
</body>

<?php
require_once('includes/footer.php');
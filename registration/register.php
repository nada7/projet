﻿<?php 

require_once("header.php");

if(!isset($_SESSION['user_id'])){
    if(isset($_POST['submit'])){
        $username = $_POST['username'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $passwordrepeat = $_POST['passwordrepeat'];
        $adress = $_POST['adresse'];
        $numtel = $_POST['numtel'];
        if($username && $email && $password && $passwordrepeat && $adress && $numtel){
            if($password==$passwordrepeat){
                $db = new PDO('mysql:host=localhost;dbname=eat_it', 'root','');
                $db->query("INSERT INTO users VALUES ('','$username','$password','$adress','$email','$numtel')");
                $erreur = false;
                $message = "Votre compte a bien été créé !";
                
            }
            else{
                $erreur = true;
                $message = "Les mot de passes ne sont pas identiques.";
            }
        }
        else{
            
        }
    }
    ?>

    <!DOCTYPE html>
    <html>
        <head>
            <title>Inscription</title>
            <link rel="stylesheet" href="style.css" />
        </head>
        <body>
            <form class="box" action="" method="post">
                <h1 class="box-title">S'inscrire</h1>
                <input type="text" class="box-input" name="username" placeholder="Nom d'utilisateur" required />
                <input type="email" class="box-input" name="email" placeholder="Email" required />
                <input type="password" class="box-input" name="password" placeholder="Mot de passe" required />
                <input type="password" class="box-input" name="passwordrepeat" placeholder="Mot de passe" required />
                <input type="text" class="box-input" name="adresse" placeholder="Adresse" required />
                <input type="tel" class="box-input" name="numtel" pattern="^[0-9]{10}$" placeholder="Numéro de téléphone" required />

                <input type="submit" name="submit" value="S'inscrire" class="box-button" />
                <p class="box-register">Déjà inscrit? <a href="login.php">Connectez-vous ici</a></p>
    <?php 
                if (! empty($message) && $erreur == true) 
                { 
                    echo "<p class='errorMessage'>".$message."</p>";
                }
                elseif(!empty($message) && $erreur == false)
                {
                    echo "<p class='successMessage'>".$message."</p>";
                }
    ?>
            </form>
        </body>
    </html>

    <?php
}
else{
    header('Location:profile.php');
}

require_once('../includes/footer.php');
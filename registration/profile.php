﻿<?php

require_once('header.php');


$db = new PDO('mysql:host=localhost;dbname=eat_it', 'root','');
$db->setAttribute(PDO::ATTR_CASE,PDO::CASE_LOWER);
$db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

$user_id = $_SESSION['user_id'];

if(!isset($user_id)){
	header('Location: login.php');
}
$select = $db->query("SELECT * FROM users WHERE id='$user_id'");

?>
</head>
	<link rel="stylesheet" type="text/css" href="../css/profile.css">
</head>
<body>
	<div class="info">
		<div class="sucess">
			<img id="imageprofile" src="../img/account.png">
			<h1>Vos informations :</h1>
			<?php

				while($s = $select->fetch(PDO::FETCH_OBJ)){?>

					<p> Pseudo : <?php echo $s->username; ?></p>
					<p> Adresse : <?php echo $s->adress; ?></p>
					<p> email: <?php echo $s->email; ?></p>
					<p> Téléphone : <?php echo '0'.$s->numtel; ?></p>
<?php
				}
?>			<a id="modif" href="modifyinfo.php">Modifier vos informations</a>
		</div>

		<div class="sucess">
			<img id="imagecommandes" src="../img/delivery.png">
			<h1>Vos commandes :</h1>
			<div id='cmd'>
				<table>
					<tr>
						<td><p class="titre"> N° de commande :</p> </td>
						<td><p> 73 </p></td>
						
					</tr>
					<tr>
						<td><p class="titre">  Montant des Articles : </p> </td>
						<td><p> 37,99€</p></td>
					</tr>
					<tr>
						<td><p class="titre"> Date de Livraison  : </p> </td>
						<td><p> 14/12/2020</p></td>
					</tr>
				</table>
			<div id="espace"> </div>
				<table>
					<tr>
						<td><p class="titre"> N° de Commande :</p> </td>
						<td><p> 41 </p></td>
						
					</tr>
					<tr>
						<td><p class="titre"> Montant des Articles : </p> </td>
						<td><p> 12,99€</p></td>
					</tr>
					<tr>
						<td><p class="titre"> Date de Livraison : </p> </td>
						<td><p> 13/12/2020</p></td>
					</tr>
				</table>
			</div>
		</div>
		<div>
			<a id="home" href="../index.php">Retour à l'acceuil</a>
			<div id="espace"></div>
			<a id="deco" href="logout.php">Déconnexion</a>
			<?php
				if($user_id == 1){?>
					<div id="espace"></div>
					<a id="deco" style="color:green;" href="../admin">Administration</a>
				<?php
				}
			?>
		</div>
	</div>
</body>

<?php
require_once('../includes/footer.php');
<?php 

require_once("header.php");

$db = new PDO('mysql:host=localhost;dbname=eat_it', 'root','');
$db->setAttribute(PDO::ATTR_CASE,PDO::CASE_LOWER);
$db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

$user_id = $_SESSION['user_id'];

$modify = $db->prepare("SELECT * FROM users WHERE id=$user_id");
$modify->execute();

$data = $modify->fetch(PDO::FETCH_OBJ);

if(!isset($user_id)){
	header('Location: login.php');
}
    if(isset($_POST['submit'])){
        $username = $_POST['username'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $passwordrepeat = $_POST['passwordrepeat'];
        $adress = $_POST['adresse'];
        $numtel = $_POST['numtel'];
        if($username && $email && $password && $passwordrepeat && $adress && $numtel){
            if($password==$passwordrepeat){
                $update = $db->prepare("UPDATE users SET username='$username',password='$password',adress='$adress',email='$email',numtel='$numtel' WHERE id=$user_id");
                $update->execute(); 
                $erreur = false;
                $message = "Vos Informations on bien été modifiées !";
            }
            else{
                $erreur = true;
                $message = "Les mot de passes ne sont pas identiques.";
            }
        }
        else{
            
        }
    }
    ?>

    <!DOCTYPE html>
    <html>
        <head>
            <title>Mofication des Informations</title>
            <link rel="stylesheet" href="style.css" />
        </head>
        <body>
            <form class="box" action="" method="post">
                <h1 class="box-title">Modifier vos Informations</h1>
                <input type="text" class="box-input" name="username" value ="<?php echo $data->username; ?>" placeholder="Nom d'utilisateur" required />
                <input type="email" class="box-input" name="email" value ="<?php echo $data->email; ?>" placeholder="Email" required />
                <input type="password" class="box-input" name="password" value ="<?php echo $data->password; ?>" placeholder="Mot de passe" required />
                <input type="password" class="box-input" name="passwordrepeat" value ="<?php echo $data->password; ?>" placeholder="Mot de passe" required />
                <input type="text" class="box-input" name="adresse" value ="<?php echo $data->adress; ?>" placeholder="Adresse" required />
                <input type="tel" class="box-input" name="numtel" pattern="^[0-9]{10}$" value ="<?php echo $data->numtel; ?>" placeholder="Numéro de téléphone" required />

                <input type="submit" name="submit" value="Modifier" class="box-button" />
                <p class="box-register">Retour à la gestion de compte? <a href="profile.php"> Juste-ici</a></p>
    <?php 
                if (! empty($message) && $erreur == true) 
                { 
                    echo "<p class='errorMessage'>".$message."</p>";
                }
                elseif(!empty($message) && $erreur == false)
                {
                    echo "<p class='successMessage'>".$message."</p>";
                }
    ?>
            </form>
        </body>
    </html>

    <?php


require_once('../includes/footer.php');
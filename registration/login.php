<?php
require_once("header.php");
require('config.php');

if(!isset($_SESSION['user_id'])){
    if (isset($_POST['submit']))
    {
        $username = $_POST['username'];
        $password = $_POST['password'];

        if($username && $password){
            $db = new PDO('mysql:host=localhost;dbname=eat_it', 'root','');
            $select = $db->query("SELECT id FROM users WHERE username='$username'");
            if($select->fetchColumn()){
                $select = $db->query("SELECT * FROM users WHERE username='$username'");
                $result = $select->fetch(PDO::FETCH_OBJ);
                $_SESSION['user_id'] = $result->id;
                $_SESSION['user_name'] = $result->username;
                $_SESSION['user_email'] = $result->email;
                $_SESSION['user_ad'] = $result->adress;
                $_SESSION['user_numtel'] = $result->numtel;
                $_SESSION['user_password'] = $result->password;
                header('Location:profile.php');
            }
            else
            {
                $message = "Le nom d'utilisateur ou le mot de passe est incorrect.";
            }
        }

    }
    ?>
        <link rel="stylesheet" type="text/css" href="style.css"> 
        <div class="container">
            <form class="box" action="" method="post" name="login">
                <h1 class="box-title">Connexion</h1>
                <input type="text" class="box-input" name="username" placeholder="Nom d'utilisateur">
                <input type="password" class="box-input" name="password" placeholder="Mot de passe">
                <input type="submit" value="Connexion " name="submit" class="box-button">
                    <div class="box-register"> 
                        <p class="box-register">Vous êtes nouveau ici? <a href="register.php">S'inscrire</a></p>
                    </div>

    <?php
            if (!empty($message)) 
            { 
                echo "<p class='errorMessage'>".$message."</p>";
            }
    ?>
                </form>
            </div>
        </body>
    </html>

    <?php
}
else{
    header('Location:profile.php');
}

require_once('../includes/footer.php');
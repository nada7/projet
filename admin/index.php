<?php

require_once("header.php");

session_start();

$PseudoAdmin = 'admin';
$MdpAdmin = 'admin';

if(isset($_POST['submit'])){

    $username = $_POST['username'];
    $password = $_POST['password'];

    if($username && $password){
        if($username == $PseudoAdmin && $password == $MdpAdmin){
            $_SESSION['username']=$username;
            header('Location: admin.php');
        }

    else {
        echo 'Identifiants erronés';
    }
}
}

?>

<link rel="stylesheet" type="text/css" href="../registration/style.css"> 
        <div class="container">
            <form class="box" action="" method="post" name="login">
                <h1 class="box-title">Administration</h1>
                <input type="text" class="box-input" name="username" placeholder="Nom d'utilisateur">
                <input type="password" class="box-input" name="password" placeholder="Mot de passe">
                <input type="submit" value="Connexion " name="submit" class="box-button">
                    <div class="box-register"> 
                        <p class="box-register">Retour à la gestion de compte? <a href="../registration/profile.php"> Juste-ici</a></p>
                    </div>
                </form>
            </div>
        </body>
    </html>
<?php

require_once("../includes/footer.php");
<?php

require_once("header.php");
session_start();

?>

<html>
    <meta charset="utf-8">
    <style> 
        body{
            font-size: 17px;
        }

        a{
            color:grey;
        }
    </style>
    <body>
        <h1>Bienvenue, <?php echo $_SESSION['username']; ?> !</h1>

        <a href="?action=add">Ajouter un produit </a> &emsp;&ensp;
        <a href="?action=modifyAndDelete">Modifier / Supprimer un produit</a>
    </br>
        <a href="?action=add_category"> Ajouter une catégorie</a> &nbsp;
        <a href="?action=modifyAndDelete_category">   Modifier / Supprimer une catégorie</a>
    </br>

<?php

if(isset($_SESSION['username'])){
    if (isset($_GET['action'])){
        if($_GET['action']=='add'){
            if(isset($_POST['submit'])){

                $title = $_POST['title'];
                $description = $_POST['description'];
                $price = $_POST['prix'];
                $category = $_POST['category'];
                $stock = $_POST['stock'];

                $img = $_FILES['img']['name'];

                $img_tmp = $_FILES['img']['tmp_name'];
                
                if(!empty($img_tmp)){
                    $image = explode('.',$img);
                    $image_ext = end($image);

                    if(in_array(strtolower($image_ext), array('png','jpg','jpeg'))=== false){
                        echo 'Veuillez rentrer une image ayant pour extension : png, jpg, jpeg';
                    }
                    else{
                        $image_size = getimagesize($img_tmp);

                        if($image_size['mime']=='image/jpeg'){
                            $image_src = imagecreatefromjpeg($img_tmp);
                        }
                        elseif($image_size['mime']=='image/png'){
                            $image_src = imagecreatefrompng($img_tmp);
                        }
                        else{
                            $image_src = false;
                            echo 'Veuillez rentrer une image valide';
                        }
                        imagejpeg($image_src,'../img/'.$title.'.jpg');
                    }
                }
                else{
                    echo "Veuillez rentrer une image";
                }

                if($title && $description && $price && $category && $stock){

                    
                    try{
                        $db = new PDO('mysql:host=localhost;dbname=eat_it', 'root','');
                        $db->setAttribute(PDO::ATTR_CASE,PDO::CASE_LOWER);
                        $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                    }
                    catch(Exception $e){
                        echo 'Une erreur est survenue';
                        die();
                    }
                    $insert = $db->prepare("INSERT INTO products (title,description,price,category,stock) VALUES ('$title','$description','$price','$category', '$stock')");
                    $insert->execute();
                    
                }
                else{
                    echo 'Veuillez remplir tout les champs';
                }
            }
?>
    
    <form action="" method="POST" enctype="multipart/form-data">
        <h3>Titre du produit :</h3>
        <input type="text" name = "title"/>

        <h3>Description du produit :</h3>
        <textarea name = "description"></textarea>

        <h3>Prix du produit :</h3>
        <input type="text" name = "prix"/>
        </br></br>
        
        <h3>Catégorie du produit :</h3>
            <select name="category"> 
            <?php 
            $db = new PDO('mysql:host=localhost;dbname=eat_it', 'root','');
            $select=$db->query("SELECT * FROM category"); 
                while($s = $select->fetch(PDO::FETCH_OBJ)){
                    ?> <option> <?php echo $s->name; ?></option><?php
                }
            ?> 
            </select>

        <h3> Stock :</h3> 
        <input type="text" name="stock"/>
        </br>

        <h3>Image :</h3>
        <input type="file" name="img"/>

        </br></br>
        <input type="submit" name="submit" value="Ajouter"/>
    </form>
<?php

    }
    
    elseif($_GET['action']=='modifyAndDelete'){
        $db = new PDO('mysql:host=localhost;dbname=eat_it', 'root','');
        $display = $db->prepare("SELECT * FROM products");
        $display->execute();
        while ($s = $display->fetch(PDO::FETCH_OBJ)){
            ?>
            </br>
            <?php
            echo $s->title."\n";
            ?>         
            <a href="?action=modify&amp;id=<?php echo $s->id; ?>">Modifier</a>
            <a href="?action=delete&amp;id=<?php echo $s->id; ?>">Supprimer</a></br>
            <?php
        }
    }
    
    elseif($_GET['action']=='modify'){

        $id = $_GET['id'];

        $db = new PDO('mysql:host=localhost;dbname=eat_it', 'root','');
        $modify = $db->prepare("SELECT * FROM products WHERE id=$id");
        $modify->execute();

        $data = $modify->fetch(PDO::FETCH_OBJ);

        ?>
        <form action="" method="POST" enctype="multipart/form-data">
            <h3>Titre du produit :</h3>
            <input value = "<?php echo $data->title; ?>" type="text" name = "title"/>

            <h3>Description du produit :</h3>
            <textarea name = "description"><?php echo $data->description; ?></textarea>

            <h3>Prix du produit :</h3>
            <input value = "<?php echo $data->price; ?>" type="text" name = "prix"/>
            </br></br>

            <h3>Catégorie du produit :</h3>
            <select> 
            <?php $select=$db->query("SELECT * FROM category"); 
                while($s = $select->fetch(PDO::FETCH_OBJ)){
                    ?> <option> <?php echo $s->name; ?></option><?php
                }
            ?> 
            </select>

            <h3> Stock :</h3> 
            <input type="text" name="stock" value="<?php echo $data->stock; ?>"/>

            <h3>Image :</h3>
            <input type="file" name="img"/>
            
            </br></br>
            <input type="submit" name="submit"/>
        </form>
        <?php

                
        if(isset($_POST['submit'])){
                    
            $title = $_POST['title'];
            $description = $_POST['description'];
            $price = $_POST['prix'];
            $stock = $_POST['stock'];

            $update = $db->prepare("UPDATE products SET title='$title', description='$description', price='$price', stock='$stock' WHERE id=$id");
            $update->execute();
            
            header('Location: admin.php?action=modifyAndDelete');
        }
    }

    elseif($_GET['action']=='delete'){
        $id = $_GET['id'];
        $db = new PDO('mysql:host=localhost;dbname=eat_it', 'root','');
        $delete = $db->prepare("DELETE FROM products WHERE id=$id");
        $delete->execute();
    }
    elseif($_GET['action']=='add_category')
    {

        if(isset($_POST['submit'])){
            $name = $_POST['name'];
            if($name){
                $db = new PDO('mysql:host=localhost;dbname=eat_it', 'root','');
                $insert = $db->prepare("INSERT INTO category VALUES ('','$name')");
                $insert->execute();
            }
            else{
                echo "Veuillez remplir tout les champs";
            }
        }

        ?>
        <form action="" method="POST">
            <h3> Titre de la catégorie :</h3>
            <input type="text" name="name"/>
            <input type="submit" name="submit" value="Ajouter"/>
        </form>
        <?php
    }

    elseif($_GET['action']=='modifyAndDelete_category')
    {
        $db = new PDO('mysql:host=localhost;dbname=eat_it', 'root','');
        $display = $db->prepare("SELECT * FROM category");
        $display->execute();
        while ($s = $display->fetch(PDO::FETCH_OBJ)){
            ?>
            </br>
            <?php 
            echo $s->name."\n";
            ?>
            <a href="?action=modify_category&amp;id=<?php echo $s->id; ?>">Modifier</a>
            <a href="?action=delete_category&amp;id=<?php echo $s->id; ?>">Supprimer</a></br>
            <?php
        }
    }

    elseif($_GET['action']=='modify_category')
    {
        $id = $_GET['id'];

        $db = new PDO('mysql:host=localhost;dbname=eat_it', 'root','');
        $modify = $db->prepare("SELECT * FROM category WHERE id=$id");
        $modify->execute();

        $data = $modify->fetch(PDO::FETCH_OBJ);

        ?>
        <form action="" method="POST">
            <h3>Nom de la catégorie :</h3> </br>
            <input value = "<?php echo $data->name; ?>" type="text" name = "name"/>
            <input type="submit" name="submit" value="Modifier"/>
        </form>
        <?php

                
        if(isset($_POST['submit'])){
                    
            $name = $_POST['name'];

            $select = $db->query("SELECT name FROM category WHERE id='$id'");

            $result = $select->fetch(PDO::FETCH_OBJ);

            $update = $db->prepare("UPDATE category SET name='$name' WHERE id=$id");
            $update->execute();

            $id = $_GET['id'];

            $update = $db->query("UPDATE products SET category='$name' WHERE category='$result->name'");
            
            header('Location: admin.php?action=modifyAndDelete_category');
        }
    }

    elseif($_GET['action']=='delete_category')
    {
        $id = $_GET['id'];
        $db = new PDO('mysql:host=localhost;dbname=eat_it', 'root','');
        $delete = $db->prepare("DELETE FROM category WHERE id=$id");
        $delete->execute();

        header('Location: admin.php?action=modifyAndDelete_category');
    }

    else{
        die('Une erreur s\' est produite.');
    }
}else {
/* si action pas defini */
}
}
else {
    header('Location: ../index.php');
}

?>
    </body>
</html>

<?php

require_once('../includes/footer.php');
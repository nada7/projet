<?php
require_once('includes/header.php');

require_once('includes/fonctions_paniers.php');


$erreur = false;

$action = (isset($_POST['action'])?$_POST['action']:(isset($_GET['action'])?$_GET['action']:null));

if($action !== null){
    if(!in_array($action, array('ajout','suppression','refresh')))
    $erreur = true;

    $l = (isset($_POST['l'])?$_POST['l']:(isset($_GET['l'])?$_GET['l']:null));
    $q = (isset($_POST['q'])?$_POST['q']:(isset($_GET['q'])?$_GET['q']:null));
    $p = (isset($_POST['p'])?$_POST['p']:(isset($_GET['p'])?$_GET['p']:null));

    $l = preg_replace('#\v#', '',$l);
    $p = floatval($p);

    if(is_array($q)){
        $qteProduit = array();
        $i = 0;
        foreach($q as $contenu){
            $qteProduit[$i++] = intval($contenu);
        }
    }
    else{
        $q = intval($q);
    }
}

if(!$erreur)
{
    switch($action){
        case "ajout" :
            ajouterArticle($l,$q,$p);
            break;

        case "suppression" :
            supprimerArticle($l);
            break;

        case "refresh" :
            for($i = 0; $i < count($qteProduit); $i++)
            {
                ModifierQTeArticle($_SESSION['panier']['libelleProduit'][$i], round($qteProduit[$i]));
            }
            break;

        default :

            break;
    }
}

?>
    <head>
        <link rel="stylesheet" type="text/css" href="css/panier.css">
    </head>
    <div id="container">
        <form method="post" action="">
            <table width="90%">
                <tr style="font-size:50px;">
                    <td colspan="4">Votre panier</td>
                </tr>
                <tr id="espace"></tr>
                <tr style="font-size:25px;">
                    <td>Libellé produit</td>
                    <td>Prix unitaire</td>
                    <td>Quantité</td>
                    <td> Action</td>
                </tr>
                <?php  

                    if(isset($_GET['deletepanier']) && $_GET['deletepanier'] == true){
                        supprimerPanier();
                    }

                    if(creationPanier()){

                        $nbProduits = count($_SESSION['panier']['libelleProduit']);

                        if($nbProduits <= 0){
                            echo '<p style="color: red; font-size:35px;">Votre panier est vide !</p>';
                        }
                        else{
                            for($i = 0; $i<$nbProduits; $i++){
                            ?>
                                <tr>
                                    <td></br><?php echo $_SESSION['panier']['libelleProduit'][$i]; ?></td> 
                                    <td></br><?php echo $_SESSION['panier']['prixProduit'][$i]; ?>€</td>
                                    <td></br><input class="qte" name="q[]" type="number" value="<?php echo $_SESSION['panier']['qteProduit'][$i]; ?>" min="0"/></td>
                                    <td>
                                        </br>
                                        <a href="panier.php?action=suppression&amp;l=<?php echo rawurlencode($_SESSION['panier']['libelleProduit'][$i]); ?>">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path d="M6 19a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V7H6v12m2.46-7.12l1.41-1.41L12 12.59l2.12-2.12l1.41 1.41L13.41 14l2.12 2.12l-1.41 1.41L12 15.41l-2.12 2.12l-1.41-1.41L10.59 14l-2.13-2.12M15.5 4l-1-1h-5l-1 1H5v2h14V4h-3.5z" fill="#626262"/></svg>
                                        </a>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                                <tr id="espace"></tr>
                                <tr id="bdp">
                                    <td colspan="2">
                                        <p>Livraison : <?php echo calculLivraison().' €'; ?></p>
                                        <p>Total : <?php echo montantGlobal().' €'; ?></p> 
                                    </td>
                                    <td colspan="2">
                                        <a id="delete" href="?deletepanier=true">Supprimer le panier</a>
                                    </br></br>
                                        <?php 
                                        if(isset($_SESSION['user_id'])){ 
                                            echo  "<a id='pay' href='achat.php'>Payer la commande</a> <?php"; 
                                        }
                                        else{ 
                                            echo "<h4 style='color:red;'>Vous devez être connecté pour payer votre commande !</h4>";
                                        } 
                                        ?>
                                    </td>
                                </tr> 
                                <tr>
                                    <td colspan="4">
                                        <input type="hidden" name="action" value="refresh"/>
                                        <input type="submit" value="Rafraichir" id="refresh"/>
                                    </td>
                                </tr>

                                <?php
                            }
                        }
                ?>
            </table>
        </form>
    </div>
<?php 

require_once('includes/footer.php');

?>
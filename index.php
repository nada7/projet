<?php 

require_once('includes/header.php');


?>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
    <link rel="stylesheet" href="https://bootswatch.com/4/lumen/bootstrap.css">
    <style> 
        .mySlides img{
            position: absolute;
            height: 91.5%;
            width:auto;
        }
    </style>
</head>
<body>
    <div class="slideshow-container">

        <div class="mySlides">
            <img src="img/img1.jpg" style="width:100%">
        </div>

        <div class="mySlides">
            <img src="img/img2.jpeg" style="width:100%">
        </div>

        <div class="mySlides">
            <img src="img/img3.jpg" style="width:100%">
        </div>

        <div class="mySlides">
            <img src="img/img4.jpg" style="width:100%">
        </div>

        <div class="mySlides">
            <img src="img/img5.jpg" style="width:100%">
        </div>
        
        <div class="mySlides">
            <img src="img/img6.jpg" style="width:100%">
        </div>

        <div class="mySlides">
            <img src="img/img8.jpg" style="width:100%">
        </div>

        <div class="mySlides">
            <img src="img/img9.jpg" style="width:100%">
        </div>
        
        <div class="mySlides">
            <img src="img/img10.jpg" style="width:100%">
        </div>

    </div>
</body>

<script>
    var slideIndex = 0;
    showSlides();

    function showSlides() {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex > slides.length) {slideIndex = 1}
    slides[slideIndex-1].style.display = "block";
    setTimeout(showSlides,4000); // Change image every 4 seconds
    }
</script>
<?php

?>
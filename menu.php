<?php
    require_once('includes/header.php');


    $db = new PDO('mysql:host=localhost;dbname=eat_it', 'root','');
    $db->setAttribute(PDO::ATTR_CASE,PDO::CASE_LOWER);
    $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);


    $categorie = $db->query("SELECT * FROM category");
    $categorie->execute();

    ?>
    <html>
    <link rel="stylesheet" type="text/css" href="css/menu.css"> 
    <link href="https://fonts.googleapis.com/css2?family=Domine:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@600&display=swap" rel="stylesheet">
    <main id="main">
    <body>
        <section>
    <?php

    while($c = $categorie->fetch(PDO::FETCH_OBJ)){
        ?>
            <div style="border:4px solid #252525; border-radius: 5px;">
                <div style="color:#101010; padding:20px 5px 20px 15px; background-color:#3aca6a; font-size:25px; font-weight:bold; text-align:center;">
                    <?php echo $c->name ?>
                </div>
            </div>
        <?php

            $select = $db->prepare("SELECT * FROM products WHERE category = '$c->name'");
            $select->execute();

            while ($s = $select->fetch(PDO::FETCH_OBJ)){
        ?>
                <div class="item">
                    <img class = "imgProd" src="img/<?php echo $s->title; ?>.jpg"/>
                    <div class="item-infos">
                        <h3> <?php echo $s->title; ?> </h3>
                        <hr class="trait">
                        <p> <?php echo $s->description; ?> </p>
                        <p class="prix"> <?php echo $s->price; ?> €</p>
                    </div>
                    <div class="sep"></div>
                    <?php
                        if($s->stock != 0)
                        {
                            ?>
                            
                            <div id="payer">
                                <a class="boutton" href="panier.php?action=ajout&amp;l=<?php echo $s->title; ?>&amp;q=1&amp;p=<?php echo $s->price; ?>" style="text-decoration:none; color: black; font-weight: bold">Ajouter au panier</a>
                            </div>
                            </br>
                            <?php
                        }
                        else{
                            ?>
                            <div id="payer">
                                <h4  style="margin-left: 75px;">Stock épuisé !</h4>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            <?php 
            
            }
             ?><br> <br> <br> <br><br><?php
    }
    ?></body> <?php
    require_once('includes/footer.php');
?>
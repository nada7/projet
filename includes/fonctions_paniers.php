<?php

function creationPanier(){

    try{
        $db = new PDO('mysql:host=localhost;dbname=eat_it', 'root','');
        $db->setAttribute(PDO::ATTR_CASE,PDO::CASE_LOWER);
        $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    }

    catch(Exception $e){
        die('Une erreur est survenue');
    }

    if(!isset($_SESSION['panier'])){
        $_SESSION['panier'] = array();
        $_SESSION['panier']['libelleProduit']=array();
        $_SESSION['panier']['qteProduit']=array();
        $_SESSION['panier']['prixProduit'] = array();
        $_SESSION['panier']['verrou']=false;
    }
    return true;
}

function ajouterArticle($libelleProduit, $qteProduit, $prixProduit){
    if(creationPanier() && !isVerouille()) {
        $position_produit = array_search($libelleProduit, $_SESSION['panier']['libelleProduit']);
        if($position_produit !== false){
            $_SESSION['panier']['qteProduit'][$position_produit] += $qteProduit;
        }
        else{
            array_push($_SESSION['panier']['libelleProduit'], $libelleProduit);
            array_push($_SESSION['panier']['qteProduit'], $qteProduit);
            array_push($_SESSION['panier']['prixProduit'], $prixProduit);
        }
    }
    else{
        echo "Un problème est survenu veuillez contacter l'administrateur du site.";
    }
}

function ModifierQTeArticle($libelleProduit, $qteProduit){
    if(creationPanier() && !isVerouille()){
        if($qteProduit > 0){
            $position_produit = array_search($libelleProduit,$_SESSION['panier']['libelleProduit']);
            if($position_produit!==false){
                $_SESSION['panier']['qteProduit'][$position_produit] = $qteProduit;
            }
        }
        else{
            supprimerArticle($libelleProduit);
        }
    }
    else{
        echo "Un problème est survenu veuillez contacter l'administrateur du site.";
    }
}

function supprimerArticle($libelleProduit){
    if(creationPanier() && !isVerouille()){
        $tmp = array();
        $tmp['libelleProduit']=array();
        $tmp['qteProduit']=array();
        $tmp['prixProduit']=array();
        $tmp['verrou']= $_SESSION['panier']['verrou'];
        for($i=0;$i<count($_SESSION['panier']['libelleProduit']); $i++){
            if($_SESSION['panier']['libelleProduit'][$i] !== $libelleProduit){
                array_push($tmp['libelleProduit'], $_SESSION['panier']['libelleProduit'][$i]);
                array_push($tmp['qteProduit'], $_SESSION['panier']['qteProduit'][$i]);
                array_push($tmp['prixProduit'], $_SESSION['panier']['prixProduit'][$i]);
            }
        }
        $_SESSION['panier'] = $tmp;
        unset($tmp);
    }
    else{
        echo "Un problème est survenu veuillez contacter l'administrateur du site.";
    }
}

function montantGlobal(){
    $total = 0;
    for($i=0;$i<count($_SESSION['panier']['libelleProduit']); $i++){
        $total += $_SESSION['panier']['qteProduit'][$i]*$_SESSION['panier']['prixProduit'][$i];
    }
    return $total + calculLivraison();
}

function supprimerPanier(){
    unset($_SESSION['panier']);
}

function isVerouille(){
    if(isset($_SESSION['panier']) && $_SESSION['panier']['verrou']){
        return true;
    }
    else{
        return false;
    }
}

function compterArticles(){
    if(isset($_SESSION['panier'])){
        return count($_SESSION['panier']['libelleProduit']);
    }
    else{
        return 0;
    }
}

function calculLivraison(){
    return 3.99;
}
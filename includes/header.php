<?php
session_start();

try{
  $db = new PDO('mysql:host=localhost;dbname=eat_it', 'root','');
  $db->setAttribute(PDO::ATTR_CASE,PDO::CASE_LOWER);
  $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
}
catch(Exception $e){
  echo 'Une erreur est survenue';
  die();
}

?>


<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
* {box-sizing: border-box;}

body {
  margin: 0;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
}
.topnav {
  overflow: hidden;
  background-color: #292b2c;
  color: whitesmoke;
  font-size: 30px;
  display: flex;
  flex-direction: row;
}

.topnav a {
  float: none;
  display: block;
  color: #5cb85c;
  text-align: center;
  margin: auto;
  padding: 5px;
  text-decoration: none;
  border-radius: 7.5px;
  justify-content: space-between;
}

.topnav a:hover {
  background-color: #5cb85c;
  color: white;
  border-radius: 7.5px;
}

.topnav .search-container {
  float: right;
  margin: auto;
  margin-right: 30px;
  padding-bottom: 7.5px;	
}

.topnav input[type=text] {
  padding: 6px;
  margin-top: 8px;
  border: none;
  font-size: 20px;
}

.topnav .search-container button {
  float: right;
  padding: 6px 8px;
  margin-top: 8px;
  background: #ddd;
  font-size: 20px;
  border: none;
  cursor: pointer;
  justify-content: flex-end;
}

.topnav .search-container button:hover {
  background: #ccc;
}

@media screen and (max-width: 600px) {
  .topnav .search-container {
    float: none;
  }
  .topnav a, .topnav input[type=text], .topnav .search-container button {
    float: none;
    display: block;
    text-align: left;
    width: 100%;
    margin: 0;
    padding: 14px;
  }
  .topnav input[type=text] {
    border: 1px solid #ccc;  
  }
}

#logo{
	margin: 10px;
	margin-left: 30px;
	float: left;
}
</style>
</head>
	<header>

		<div class="topnav">
			<img id="logo" src="img/logo.svg" height="60px"/>
			<a href="index.php">Accueil</a>
			<a  href="menu.php">Menu</a>
      <a  href="panier.php">Panier</a>
      <?php if(!isset($_SESSION['user_id'])){ ?>
        <a href="registration/register.php">S'inscrire</a>
        <a href="registration/login.php">Se connecter</a>
      <?php
      }
      else{?>
        <a  href="registration/profile.php">Compte</a>
      <?php
      }?>
			<div class="search-container">
				<form action="/action_page.php">
					<input type="text" placeholder="Commandez chez nous !" name="search">
					<button type="submit"><i class="fa fa-search"></i></button>
				</form>
			</div>
		</div>


	</header>
</html>
